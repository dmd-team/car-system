INSERTS="inserts"
HOST="127.0.0.1"
PORT="5432"
USERNAME="postgres"
DBNAME="postgres"

export PGPASSWORD='1'

psql $DBNAME < dump_init.sql --host=$HOST --port=$PORT --username=$USERNAME -q

psql $DBNAME < $INSERTS/car_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/plug_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/charging_station_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/location_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/payment_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/workshop_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/provider_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/car_states_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/car_type_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/car_part_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/fits_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/customer_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/charge_instance_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/supports_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/parts_available_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/service_instance_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/car_order_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/supply_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/supply_content_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
psql $DBNAME < $INSERTS/parts_used_dump.sql --host=$HOST --port=$PORT --username=$USERNAME -q
