

  -- Car_Type
  insert into car_type (id, battery_capacity, plug_id, price_per_hour, power) values (0,55,3,13,48);
  insert into car_type (id, battery_capacity, plug_id, price_per_hour, power) values (1,65,0,89,59);
  insert into car_type (id, battery_capacity, plug_id, price_per_hour, power) values (2,50,0,21,48);
  insert into car_type (id, battery_capacity, plug_id, price_per_hour, power) values (3,60,1,9,60);
  insert into car_type (id, battery_capacity, plug_id, price_per_hour, power) values (4,55,3,59,16);
