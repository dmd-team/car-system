
-- Workshop
insert into workshop (id, open_time, close_time, gps_location) values (0,'9:20:06','20:20:06','-31.58452 84.76691');
insert into workshop (id, open_time, close_time, gps_location) values (1,'9:02:53','19:02:53','71.18506 -20.99575');
insert into workshop (id, open_time, close_time, gps_location) values (2,'8:28:39','18:28:39','52.68903 125.90508');
insert into workshop (id, open_time, close_time, gps_location) values (3,'9:39:28','20:39:28','-44.16828 -86.5861');
insert into workshop (id, open_time, close_time, gps_location) values (4,'8:23:52','19:23:52','23.40144 -115.5364');
insert into workshop (id, open_time, close_time, gps_location) values (5,'7:26:29','15:26:29','78.36344 127.55376');
insert into workshop (id, open_time, close_time, gps_location) values (6,'7:05:53','19:05:53','68.5471 99.22959');
insert into workshop (id, open_time, close_time, gps_location) values (7,'9:26:34','18:26:34','7.57823 88.5260');
insert into workshop (id, open_time, close_time, gps_location) values (8,'7:00:42','15:00:42','-54.68134 142.57505');
insert into workshop (id, open_time, close_time, gps_location) values (9,'8:30:18','17:30:18','19.68370 37.64621');
insert into workshop (id, open_time, close_time, gps_location) values (10,'7:23:33','19:23:33','39.97844 11.72944');
insert into workshop (id, open_time, close_time, gps_location) values (11,'7:15:56','17:15:56','18.90512 52.97578');
insert into workshop (id, open_time, close_time, gps_location) values (12,'7:48:21','15:48:21','30.52507 -167.55775');
insert into workshop (id, open_time, close_time, gps_location) values (13,'9:05:09','18:05:09','25.80123 -68.31540');
insert into workshop (id, open_time, close_time, gps_location) values (14,'7:54:04','17:54:04','2.24778 -144.93075');
insert into workshop (id, open_time, close_time, gps_location) values (15,'9:44:09','21:44:09','-47.28243 162.62563');
insert into workshop (id, open_time, close_time, gps_location) values (16,'7:30:53','16:30:53','67.60083 108.62832');
insert into workshop (id, open_time, close_time, gps_location) values (17,'7:31:54','17:31:54','54.16185 -129.82728');
insert into workshop (id, open_time, close_time, gps_location) values (18,'7:28:17','15:28:17','-61.40357 -98.33531');
insert into workshop (id, open_time, close_time, gps_location) values (19,'8:16:15','19:16:15','-40.2700 111.79578');
