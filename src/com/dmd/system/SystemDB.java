package com.dmd.system;

import java.sql.*;
import java.sql.Date;
import java.util.*;

import com.dmd.system.Utils;
import javafx.util.Pair;

import javax.rmi.CORBA.Util;

public class SystemDB extends Database {

    /**
     * Returns amount of cars in the DB
     */
    public int getTaxisAmount() {
        String sql = " SELECT COUNT(*) FROM car";
        int count = 0;

        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return count;
    }

    public String query1(String username, Date date) {
        StringBuilder out = new StringBuilder();
        String sql = "SELECT A.id FROM (" +
                "(SELECT * FROM car WHERE car.color='Red' AND car.number SIMILAR TO 'AN%') AS A " +
                "INNER JOIN " +
                "(SELECT * FROM car_order WHERE car_order.username='" + username +
                "' AND DATE(car_order.start_datetime)='" + date + "' ) AS B ON A.id=B.car_id);";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt(1);
                out.append(id).append("\n");
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return out.toString();
    }


    public String query2(Date date) {
        Timestamp dayStart = Utils.getDayStartTS(date);
        Timestamp dayEnd = Utils.getDayEndTS(date);

        int[] sockets = new int[24];
        Arrays.fill(sockets, 0);
        String sql = " SELECT start_datetime, end_datetime " +
                " FROM charge_instance " +
                " WHERE start_datetime::date='" + date +
                "' OR end_datetime::date='" + date +
                "' ORDER BY start_datetime; ";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Timestamp start = rs.getTimestamp(1);
                Timestamp end = rs.getTimestamp(2);

                if (start.compareTo(dayStart) < 0) {
                    start = dayStart;
                }
                if (end.compareTo(dayEnd) > 0) {
                    end = dayEnd;
                }

                for (int i = Utils.getHours(start); i <= Utils.getHours(end); i++) {
                    sockets[i]++;
                }
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        StringBuilder out = new StringBuilder();
        for (int i = 0; i < 24; i++) {
            out.append(String.format("%2d", i)).append(" ").append(sockets[i]).append("\n");
        }
        return out.toString();
    }


    public String query3(Date weekStartDate) {
        Timestamp weekStartTS = Utils.getDayStartTS(weekStartDate);
        Timestamp weekEndTS = Utils.getDayEndTS(Utils.getNextDate(weekStartDate, 7));

        HashSet morning = new HashSet();
        HashSet afternoon = new HashSet();
        HashSet evening = new HashSet();

        int count = getTaxisAmount();

        String sql = " SELECT car_id, start_datetime, end_datetime " +
                " FROM car_order " +
                " WHERE start_datetime>='" + weekStartTS +
                "' AND end_datetime<='" + weekEndTS +
                "' ORDER BY start_datetime; ";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt(1);
                Timestamp start = rs.getTimestamp(2);
                Timestamp end = rs.getTimestamp(3);

                if (start.compareTo(weekStartTS) < 0) {
                    start = weekStartTS;
                }
                if (end.compareTo(weekEndTS) > 0) {
                    end = weekEndTS;
                }

                if ((Utils.getHours(start) >= 7 && Utils.getHours(start) <= 10) ||
                        (Utils.getHours(end) >= 7 && Utils.getHours(end) <= 10) ||
                        (Utils.getHours(start) <= 7 && Utils.getHours(end) >= 10)) {
                    morning.add(id);
                }
                if ((Utils.getHours(start) >= 12 && Utils.getHours(start) <= 14) ||
                        (Utils.getHours(end) >= 12 && Utils.getHours(end) <= 14) ||
                        (Utils.getHours(start) <= 12 && Utils.getHours(end) >= 14)) {
                    afternoon.add(id);
                }
                if ((Utils.getHours(start) >= 17 && Utils.getHours(start) <= 19) ||
                        (Utils.getHours(end) >= 17 && Utils.getHours(end) <= 19) ||
                        (Utils.getHours(start) <= 17 && Utils.getHours(end) >= 19)) {
                    evening.add(id);
                }
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        int mor = 100 * morning.size() / count;
        int aft = 100 * afternoon.size() / count;
        int eve = 100 * evening.size() / count;

        StringBuilder out = new StringBuilder();
        out.append("Morning    - Afternoon  - Evening   \n");
        out.append(String.format("%10d%10d%10d", mor, aft, eve));
        return out.toString();
    }


    public String query4(String username) {
        Timestamp curdate = Timestamp.valueOf("2018-11-30 23:59:59.000");
        Timestamp startTS = Utils.getDayStartTS(Utils.getNextDate(new Date(curdate.getTime()), -30));

        int count = 0;
        int discount = 0;

        String sql = "SELECT COUNT(B.datetime) FROM (( SELECT payment_id " +
                " FROM car_order " +
                " WHERE start_datetime>='" + startTS +
                "' AND username='" + username +
                "') AS A INNER JOIN (" +
                "SELECT * FROM payment) AS B ON A.payment_id=B.id); ";

        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                count = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        sql = "SELECT COUNT(DISTINCT B.datetime) FROM (( SELECT payment_id " +
                " FROM car_order " +
                " WHERE start_datetime>='" + startTS +
                "' AND username='" + username +
                "') AS A INNER JOIN (" +
                "SELECT * FROM payment) AS B ON A.payment_id=B.id) ";

        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                discount = rs.getInt(1);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        StringBuilder out = new StringBuilder();
        if (count == discount) {
            out.append("Nothing was doubled last month\n");
        } else {
            out.append("Something was doubled last month\nContact for further processing\n");
        }
        return out.toString();
    }

    public String query5(Date date) {
        long count = 0;
        double totalDistance = 0;
        long totalDuration = 0;

        String sql = " SELECT A.car_id, start_datetime, end_datetime, start_gps,gps_location" +
                " FROM ( (SELECT car_id, start_datetime,end_datetime, start_gps " +
                " FROM car_order " +
                " WHERE start_datetime::date='" + date +
                "' AND end_datetime::date='" + date +
                "') AS A INNER JOIN (SELECT car_id, gps_location, datetime FROM car_state WHERE datetime::date='" + date + "') " +
                "AS B ON A.car_id=B.car_id AND DATE_PART('HOUR', A.start_datetime)=DATE_PART('HOUR', B.datetime)); ";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int carid = rs.getInt(1);
                Timestamp start = rs.getTimestamp(2);
                Timestamp end = rs.getTimestamp(3);
                String orderStartGPS = rs.getString(4);
                String carGPS = rs.getString(5);

                count++;

                totalDistance += Utils.distance(orderStartGPS, carGPS);
                totalDuration += Utils.duration(start, end);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        double avedist = totalDistance / count;
        double avedur = totalDuration / count / 1000;

        StringBuilder out = new StringBuilder();
        out.append(String.format("Average duration: %d:%d:%d", (int) avedur / 3600, (int) avedur / 60 % 60, (int) avedur % 60)).append("\n");
        out.append("Average distance: ").append((int) avedist);
        return out.toString();
    }


    public String query6() {
        HashMap<String, Integer> morning = new HashMap<String, Integer>();
        HashMap<String, Integer> afternoon = new HashMap<String, Integer>();
        HashMap<String, Integer> evening = new HashMap<String, Integer>();

        double distMorning = 0;
        double distAft = 0;
        double distEve = 0;

        String sql = " SELECT start_gps,end_gps, start_datetime, end_datetime " +
                " FROM car_order;";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                String startGPS = rs.getString(1);
                String endGPS = rs.getString(2);
                Timestamp start = rs.getTimestamp(3);
                Timestamp end = rs.getTimestamp(4);


                if ((Utils.getHours(start) >= 7 && Utils.getHours(start) <= 10) ||
                        (Utils.getHours(end) >= 7 && Utils.getHours(end) <= 10) ||
                        (Utils.getHours(start) <= 7 && Utils.getHours(end) >= 10)) {
                    if (morning.get(startGPS) == null) {
                        morning.put(startGPS, 1);
                    } else {
                        morning.put(startGPS, morning.get(startGPS) + 1);
                    }

                    distMorning += Utils.distance(startGPS, endGPS);
                }
                if ((Utils.getHours(start) >= 12 && Utils.getHours(start) <= 14) ||
                        (Utils.getHours(end) >= 12 && Utils.getHours(end) <= 14) ||
                        (Utils.getHours(start) <= 12 && Utils.getHours(end) >= 14)) {
                    if (afternoon.get(startGPS) == null) {
                        afternoon.put(startGPS, 1);
                    } else {
                        afternoon.put(startGPS, morning.get(startGPS) + 1);
                    }

                    distAft += Utils.distance(startGPS, endGPS);
                }
                if ((Utils.getHours(start) >= 17 && Utils.getHours(start) <= 19) ||
                        (Utils.getHours(end) >= 17 && Utils.getHours(end) <= 19) ||
                        (Utils.getHours(start) <= 17 && Utils.getHours(end) >= 19)) {
                    if (evening.get(startGPS) == null) {
                        evening.put(startGPS, 1);
                    } else {
                        evening.put(startGPS, morning.get(startGPS) + 1);
                    }

                    distEve += Utils.distance(startGPS, endGPS);
                }


            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        List<Map.Entry<String, Integer>> list = new ArrayList(morning.entrySet());
        list.sort(Map.Entry.comparingByValue());

        StringBuilder out = new StringBuilder();
        out.append("Morning:\nTop pick-up locations:\n");
        out.append(list.get(list.size() - 1).getKey()).append("\n");
        out.append(list.get(list.size() - 1).getValue()).append("\n");
        out.append(list.get(list.size() - 2).getKey()).append("\n");
        out.append(list.get(list.size() - 2).getValue()).append("\n");
        out.append(list.get(list.size() - 3).getKey()).append("\n");
        out.append(list.get(list.size() - 3).getValue()).append("\n");

        out.append("Total distance:").append("\n");
        out.append(distMorning).append("\n");

        list = new ArrayList(afternoon.entrySet());
        list.sort(Map.Entry.comparingByValue());

        out.append("\nAfternoon:\nTop pick-up locations:\n");
        out.append(list.get(list.size() - 1).getKey()).append("\n");
        out.append(list.get(list.size() - 1).getValue()).append("\n");
        out.append(list.get(list.size() - 2).getKey()).append("\n");
        out.append(list.get(list.size() - 2).getValue()).append("\n");
        out.append(list.get(list.size() - 3).getKey()).append("\n");
        out.append(list.get(list.size() - 3).getValue()).append("\n");

        out.append("Total distance:").append("\n");
        out.append(distAft).append("\n");

        list = new ArrayList(evening.entrySet());
        list.sort(Map.Entry.comparingByValue());

        out.append("\nEvening:\nTop pick-up locations:\n");
        out.append(list.get(list.size() - 1).getKey()).append("\n");
        out.append(list.get(list.size() - 1).getValue()).append("\n");
        out.append(list.get(list.size() - 2).getKey()).append("\n");
        out.append(list.get(list.size() - 2).getValue()).append("\n");
        out.append(list.get(list.size() - 3).getKey()).append("\n");
        out.append(list.get(list.size() - 3).getValue()).append("\n");

        out.append("Total distance:").append("\n");
        out.append(distEve).append("\n");

        return out.toString();
    }

    public String query7() {
        Timestamp curdate = Timestamp.valueOf("2018-11-30 23:59:59.000");
        Timestamp startTS = Utils.getDayStartTS(Utils.getNextDate(new Date(curdate.getTime()), -60));

        HashMap<Integer, Integer> cars = new HashMap<>();

        String sql = " SELECT car_id " +
                " FROM car_order WHERE start_datetime>='" + startTS + "';";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {

                int carId = rs.getInt(1);

                if (cars.get(carId) == null) {
                    cars.put(carId, 1);
                } else {
                    cars.put(carId, (cars.get(carId) + 1));
                }
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        ArrayList<Map.Entry<Integer, Integer>> list = new ArrayList<>(cars.entrySet());
        list.sort(Map.Entry.comparingByValue());

        StringBuilder out = new StringBuilder();
        out.append("The least used 10% of cars are:\n");
        for (int i = 0; i <= 0.1f * list.size(); i++) {
            out.append("ID: ").append(list.get(i).getKey()).append("\n");
        }

        return out.toString();
    }

    public String query8(Date startDate) {
        Timestamp startTS = Utils.getDayStartTS(startDate);
        Timestamp endTS = Utils.getDayEndTS(Utils.getNextDate(startDate, 30));

        StringBuilder out = new StringBuilder();

        String sql = " SELECT  username, COUNT(*) FROM ((SELECT DISTINCT username, car_id, start_datetime " +
                " FROM car_order WHERE start_datetime>='" + startTS + "' AND end_datetime<='" + endTS + "')" +
                " AS CO INNER JOIN (SELECT car_id, start_datetime FROM charge_instance) AS CI ON " +
                "CO.car_id=CI.car_id AND CO.start_datetime::date=CI.start_datetime::date) " +
                "GROUP BY username;";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                String username = rs.getString(1);
                int count = rs.getInt(2);

                out.append(username + ": ");
                out.append(count);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }
        return out.toString();
    }

    public String query9() {

        StringBuilder out = new StringBuilder();

        HashMap<Integer, Pair<Integer, Integer>> map = new HashMap<>();

        Timestamp curdate = Timestamp.valueOf("2018-11-30 23:59:59.000");
        Timestamp startTS = Utils.getDayStartTS(Utils.getNextDate(new Date(curdate.getTime()), -30));

        String sql = "SELECT  workshop_id, part_id, COUNT(*) FROM ((SELECT id, workshop_id FROM supply " +
                "WHERE datetime>='" + startTS + "' AND datetime<='" + curdate + "') AS S INNER JOIN " +
                "(SELECT supply_id, part_id FROM supply_content) AS SC " +
                "ON S.id=SC.supply_id) " +
                "GROUP BY workshop_id,part_id";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int wid = rs.getInt(1);
                int pid = rs.getInt(2);
                int times = rs.getInt(3);

                if (map.get(wid) == null)
                    map.put(wid, new Pair<>(pid, times));
                else if (map.get(wid).getValue() < times)
                    map.put(wid, new Pair<>(pid, times));
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }


        for (Map.Entry<Integer, Pair<Integer, Integer>> entry : map.entrySet()) {
            out.append("Workshop with id: " + entry.getKey() + "  most often requires car part with id: " + entry.getValue().getKey() + "\n");
            out.append("Last month: " + entry.getValue().getValue() + " times\n\n");
        }
        return out.toString();
    }

    public String query10() {
        Timestamp curdate = Timestamp.valueOf("2018-11-30 23:59:59.000");
        Timestamp startTS = Utils.getDayStartTS(Utils.getNextDate(new Date(curdate.getTime()), -30));

        StringBuilder out = new StringBuilder();

        HashMap<Integer, Long> map = new HashMap<>();

        String sql = "SELECT car.type_id, SUM(X) FROM (car INNER JOIN " +
                "(SELECT car_id,abs(extract(epoch from end_datetime - start_datetime)/3600)*charge_price/power AS X FROM (" +
                "(SELECT car_id, station_id,start_datetime,end_datetime FROM charge_instance WHERE start_datetime>='" + startTS + "') " +
                "AS CI " +
                "INNER JOIN (SELECT id, charge_price,power FROM charging_station) AS CS " +
                "ON CI.station_id=CS.id)) AS B ON car.id=B.car_id)" +
                "GROUP BY car.type_id";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int type = rs.getInt(1);
                long money = rs.getLong(2);
                if (map.get(type) == null)
                    map.put(type, money);
                else
                    map.put(type, map.get(type) + money);

            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }

        sql = "SELECT car.type_id, SUM(service_instance.cost) FROM (car INNER JOIN service_instance " +
                "ON car.id = service_instance.car_id) " +
                "GROUP BY car.type_id";
        try {
            Statement st = getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int type = rs.getInt(1);
                long money = rs.getLong(2);
                if (map.get(type) == null)
                    map.put(type, money);
                else
                    map.put(type, map.get(type) + money);
            }
        } catch (SQLException e) {
            System.out.println("DB error: " + e);
        }


        for (Map.Entry entry : map.entrySet()) {
            out.append("Type id: " + entry.getKey() + " consumes money: " + entry.getValue() + " for charges and services \n");
        }
        return out.toString();
    }

}
