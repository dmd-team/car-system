package com.dmd.system;

import javax.xml.crypto.Data;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

public class Utils {
    public static int getHours(Timestamp ts) {
        return (int) (ts.getTime() / 60 / 60) % 24;
    }

    public static Timestamp getDayStartTS(Date date) {
        return new Timestamp(date.getTime() / (60 * 60 * 24 * 1000) * (60 * 60 * 24 * 1000));
    }

    public static Date getNextDate(Date date, int offset) {


        return new Date(date.getTime() + (offset * 24L * 60L * 60L * 1000L));
    }

    public static Timestamp getDayEndTS(Date date) {
        return new Timestamp(date.getTime() + 24 * 60 * 60 * 1000 - 1);
    }

    public static long duration(Timestamp st, Timestamp end) {
        return end.getTime() - st.getTime();
    }

    public static double distance(String startGPS, String endGPS) {
        String lat[] = startGPS.split(" ");
        String lon[] = endGPS.split(" ");

        double lat1 = Double.parseDouble(lat[0]);
        double lat2 = Double.parseDouble(lat[1]);
        double lon1 = Double.parseDouble(lon[0]);
        double lon2 = Double.parseDouble(lon[1]);

        return Math.abs(distanceInKmBetweenEarthCoordinates(lat1, lon1, lat2, lon2));
    }

    public static double distanceInKmBetweenEarthCoordinates(double lat1, double lon1, double lat2, double lon2) {
        long earthRadiusKm = 6371;

        double dLat = degreesToRadians(lat2 - lat1);
        double dLon = degreesToRadians(lon2 - lon1);

        lat1 = degreesToRadians(lat1);
        lat2 = degreesToRadians(lat2);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return earthRadiusKm * c;
    }

    public static double degreesToRadians(double degrees) {
        return degrees * Math.PI / 180;
    }

}
