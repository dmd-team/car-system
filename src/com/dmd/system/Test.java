package com.dmd.system;

import java.lang.reflect.Array;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Scanner;

public class Test {
    public static void main(String[] args) {

        SystemDB db = new SystemDB();
        Scanner in = new Scanner(System.in);

        System.out.println("Welcome to our database interface\n10 selected queries are implemented\n" +
                "type number 1..10 or q to exit\n\n");

        while (true) {
            String nextCommand = in.next();
            switch (nextCommand) {
                case "1": {
                    System.out.println("Enter username and date");
                    String username = in.next();
                    String date;
                    try {
                        date = in.next();
                        System.out.println(db.query1(username, Date.valueOf(date)));
                    } catch (Exception e) {
                        //System.out.println(e.print...);
                    }
                    break;
                }
                case "2": {
                    System.out.println("Enter date");
                    String date;
                    try {
                        date = in.next();
                        System.out.println(db.query2(Date.valueOf(date)));
                    } catch (Exception e) {
                        //System.out.println(e.print...);
                    }
                    break;
                }
                case "3": {
                    System.out.println("Enter date");
                    String date;
                    try {
                        date = in.next();
                        System.out.println(db.query3(Date.valueOf(date)));
                    } catch (Exception e) {
                        //System.out.println(e.print...);
                    }
                    break;
                }
                case "4": {
                    System.out.println("Enter username and date");
                    String username = in.next();
                    System.out.println(db.query4(username));
                    break;
                }
                case "5": {

                    System.out.println("Enter date");
                    String date;
                    try {
                        date = in.next();
                        System.out.println(db.query5(Date.valueOf(date)));
                    } catch (Exception e) {
                        //System.out.println(e.print...);
                    }
                    break;
                }
                case "6": {
                    System.out.println(db.query6());
                    break;
                }
                case "7": {
                    System.out.println(db.query7());
                    break;
                }
                case "8": {
                    System.out.println("Enter date");
                    String date;
                    try {
                        date = in.next();
                        System.out.println(db.query8(Date.valueOf(date)));
                    } catch (Exception e) {
                        //System.out.println(e.print...);
                    }
                    break;
                }
                case "9": {
                    System.out.println(db.query9());
                    break;
                }
                case "10": {
                    System.out.println(db.query10());
                    break;
                }
                case "q": {
                    return;
                }
            }
        }

    }

}
