package com.dmd.system;

import java.sql.Connection;
import java.sql.DriverManager;


public class Database {
    private final static String USER = "postgres";
    private final static String PASSWORD = "1";
    private final static String DRIVER = "org.postgresql.Driver";

    private Connection connection = null;


    public Database() {

        String url = "jdbc:postgresql://localhost:5432/postgres" + "?user=" + USER + "&password=" + PASSWORD + "&ssl=false";
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(url);
        } catch (Exception e) {
            System.out.println("DB error: " + e);
        }
    }

    public Connection getConnection() {
        return connection;
    }



}
